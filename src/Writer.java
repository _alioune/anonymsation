import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Writer {
	// fonction pour �crire sur un fichier CSV apr�s transformation
	public static void writerInCSV(File file, Iterator<String[]> transformed) {
		try( BufferedWriter br=new BufferedWriter(new FileWriter(file))){
	    	   while (transformed.hasNext()) {
	               System.out.print("   ");
	               String[] tab=transformed.next();
	               String line=Arrays.toString(tab);
	               System.out.println(line);
	               br.write(line+ "\n");
	           }
	       }catch(IOException e) {
	    	   e.printStackTrace();
	       }
	}
	//�crit sur un fichier Excel apr�s transformation
	public static void writeInExcel(File file, Iterator<String[]> transformed) {
		
	}
	// Ecrit une liste dans un fichier CSV
	public static void writeListInCSV(List<String> list, File f) {
		try( BufferedWriter br=new BufferedWriter(new FileWriter(f))){
	             list.stream()
	             	 .forEach(line->
	             			 		{	
	             			 			try {
											br.write(line);
											br.write("\n");
										} catch (IOException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
	             			 		});
	       }catch(IOException e) {
	    	   e.printStackTrace();
	       }
	}
	

}
