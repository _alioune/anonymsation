import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class ExcelReader {
    
    private static List<String> lines=new ArrayList<>();
    private static String val;
    public static String getVal() {
		return val;
	}

	public static void setVal(String val) {
		ExcelReader.val = val;
	}

	public static List<String> lines(String fichierDeDepart) {

        // Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook;
		try {
			workbook = WorkbookFactory.create(new File(fichierDeDepart));
			// Retrieving the number of sheets in the Workbook
	        System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");
	        /*
	           ==================================================================
	           Iterating over all the rows and columns in a Sheet 
	           ==================================================================
	        */
	        // Getting the Sheet at index zero
	        Sheet sheet = workbook.getSheetAt(0);
	     // 3. Or you can use Java 8 forEach loop with lambda
	        System.out.println("\n\nIterating over Rows and Columns using Java 8 forEach with lambda\n");
	        sheet.forEach(row -> 
	        {
	        	StringJoiner sj=new StringJoiner(";");
	            row.forEach(
	            		cell -> 
	            		{
	            			printCellValue(cell);
	            			sj.add(cell.toString());
	            		}
	            		);
	            lines.add(sj.toString());
	            System.out.println();
	        });
	        // Closing the workbook
	        workbook.close();
	        val=lines.get(0);
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return lines;
    }
	

    public static List<String> getLines() {
		return lines;
	}

	public static void setLines(List<String> lines) {
		ExcelReader.lines = lines;
	}

	private static void printCellValue(Cell cell) {
        switch (cell.getCellTypeEnum()) {
            case BOOLEAN:
                System.out.print(cell.getBooleanCellValue());
                break;
            case STRING:
                System.out.print(cell.getRichStringCellValue().getString());
                break;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    System.out.print(cell.getDateCellValue());
                } else {
                    System.out.print(cell.getNumericCellValue());
                }
                break;
            case FORMULA:
                System.out.print(cell.getCellFormula());
                break;
            case BLANK:
                System.out.print("");
                break;
            default:
                System.out.print("");
        }

        System.out.print("\t");
    }
}
