import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;
import org.deidentifier.arx.ARXAnonymizer;
import org.deidentifier.arx.ARXConfiguration;
import org.deidentifier.arx.ARXResult;
import org.deidentifier.arx.AttributeType.Hierarchy;
import org.deidentifier.arx.AttributeType.Hierarchy.DefaultHierarchy;
import org.deidentifier.arx.Data;
import org.deidentifier.arx.criteria.KAnonymity;

public class Main extends Example {
	public static final String file = "./file_pour_test.xlsx";
	private static String firstLine; //va contenir la premi�re ligne du fichier
	private static int taille; // va contenir le nombre d'�l�ments de la premi�re ligne 
	                          // pour la cr�ation de la hi�rarchie (lors de l'automatisation)
	
    public String getFirstLine() {
		return firstLine;
	}
	public void setFirstLine(String firstLine) {
		Main.firstLine = firstLine;
	}
	public static void main(String[] args) throws IOException {
    	// Define data-- Si c'est un fichier CSV utiliser cette m�thode directement
    		File backup=new File("backup.csv");
    		Writer.writeListInCSV(ExcelReader.lines(file), backup);
    		firstLine=ExcelReader.getVal();
    		
       // essayer de combiner les deux projets pour que automatiser �a apr�s d�tection type
        // Define hierarchies
    	taille=firstLine.split(";").length;
    	// Le s�parateur peut-�tre param�trer gr�ce � une variable (un char)
		
        Data data = Data.create(backup, Charset.defaultCharset(), ';');
       
  /*  	for(int i=0;i<taille;i++) {
    		DefaultHierarchy dh= Hierarchy.create(); //au cas o� vous voulez automatiser 
    		String[] tab=firstLine.split(";");       // la cr�ation de hi�rarchie
    		ExcelReader.getLines()                   // � continuer
    		           .stream()
    		           .forEach(
    		        		   line->{
    		        	   				
    		           });
    					
    		
    	}*/
        DefaultHierarchy age = Hierarchy.create();
        age.add("34.0", "<50", "*");
        age.add("45.0", "<50", "*");
        age.add("66.0", ">=50", "*");
        age.add("70.0", ">=50", "*");

        DefaultHierarchy gender = Hierarchy.create();
        gender.add("male", "*");
        gender.add("female", "*");

        // Only excerpts for readability
        DefaultHierarchy zipcode = Hierarchy.create();
        zipcode.add("81667", "8166*", "816**", "81***", "8****", "*****");
        zipcode.add("81675", "8167*", "816**", "81***", "8****", "*****");
        zipcode.add("81925", "8192*", "819**", "81***", "8****", "*****");
        zipcode.add("81931", "8193*", "819**", "81***", "8****", "*****");

        data.getDefinition().setAttributeType("age", age);
        data.getDefinition().setAttributeType("gender", gender);
        data.getDefinition().setAttributeType("zipcode", zipcode);

        // Create an instance of the anonymizer
        ARXAnonymizer anonymizer = new ARXAnonymizer();
        ARXConfiguration config = ARXConfiguration.create();
        config.addPrivacyModel(new KAnonymity((int)(ExcelReader.getLines().size()*0.50)));
        config.setSuppressionLimit(0d);
        ARXResult result = anonymizer.anonymize(data, config);
        /* Suppression du fichier backup */
        if(backup.delete()) {
       	 System.out.println("File deleted successfully"); 
       } 
       else
       { 
           System.out.println("Failed to delete the file"); 
       } 
   	
     // Print info
        printResult(result, data);
        // Process results
        System.out.println(" - Transformed data:");
        Iterator<String[]> transformed = result.getOutput(false).iterator();
        File f=new File("test.csv");
        Writer.writerInCSV(f, transformed); //� tester � l'aide d'un boolean 
                                             // pour savoir si le fichier de d�part
                                             // est en excel ou csv 
                                             //m�thode writeInExcel � �crire
        
    }
}